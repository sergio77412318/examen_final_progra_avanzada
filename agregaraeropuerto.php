<?php
require 'db.php';
$message = '';
if (isset ($_POST['nombre']) && isset ($_POST['ciudad'])  && isset($_POST['ubicacion']) && isset($_POST['contacto']) && isset($_POST['temperatura']) && isset($_POST['urlweb'])) {
  $nombre = $_POST['nombre'];
  $ciudad = $_POST['ciudad'];
  $ubicacion=$_POST['ubicacion'];
  $contacto = $_POST['contacto'];
  $temperatura = $_POST['temperatura'];
  $urlweb = $_POST['urlweb'];
  $sql = 'INSERT INTO aeropuerto(nombre, ciudad,ubicacion, contacto,temperatura,urlweb) VALUES(:nombre, :ciudad,:ubicacion, :contacto, :temperatura, :urlweb)';
  $statement = $connection->prepare($sql);
  if ($statement->execute([':nombre' => $nombre, ':ciudad'=>$ciudad, ':ubicacion' => $ubicacion, ':contacto'=>$contacto , ':temperatura'=>$temperatura, ':urlweb'=>$urlweb ])) {
    $message = 'Agregado Correctamente';
  }



}


 ?>
<?php require 'include/navadmin.php'; ?>
<style>
input:invalid {
  border: 1px solid red;
}

input:valid {
  border: 1px solid green;
}
</style>
<div class="container">
  <div class="card mt-5">
    <div class="card-header">
      <h2  style="color:white">Agregar</h2>
    </div>
    <div class="card-body">
      <?php if(!empty($message)): ?>
        <div class="alert alert-success">
          <?= $message; ?>
        </div>
      <?php endif; ?>
      <form method="post">
        <div class="form-group">
          <label for="nombre"  style="color:white">Nombre</label><br>
          <input type="text" name="nombre" id="nombre" class="col-md-6" pattern="<?php [':nombre' => $nombre]?>"   required><br>
        </div>
        <div class="form-group">
          <label for="ciudad"  style="color:white">Ciudad</label><br>
          <input type="text" name="ciudad" id="ciudad" class="col-md-6" pattern="<?php [':ciudad' => $ciudad]?>"   required><br>
        </div>
        <div class="form-group">
          <label for="ubicacion"  style="color:white">Ubicacion</label><br>
          <input type="text" name="ubicacion" id="ubicacion" class="col-md-6" pattern="<?php [':ubicacion' => $ubicacion]?>"   required><br>
        </div>
        <div class="form-group">
          <label for="contacto"  style="color:white">Contacto</label><br>
          <input type="text" name="contacto" id="contacto" class="col-md-6" pattern="<?php [':contacto' => $contacto]?>"   required><br>
        </div>
        <div class="form-group">
          <label for="temperatura"  style="color:white">Temperatura Promedio</label><br>
          <input type="text" name="temperatura" id="temperatura" class="col-md-6" pattern="<?php [':temperatura' => $temperatura]?>"   required><br>
        </div>
        <div class="form-group">
          <label for="urlweb"  style="color:white">Para mayor informacion visita</label><br>
          <input type="text" name="urlweb" id="urlweb" class="col-md-6" pattern="<?php [':urlweb' => $urlweb]?>"   required><br>
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-info">Agregar</button>
        </div>
      </form>
    </div>
  </div>
  <?php require 'include/footer.php'; ?>
</div>
