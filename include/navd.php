<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Tangerine">
        <link rel="stylesheet" href="css/bootstrap.css">
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    <title>Administrador</title>
</head>
<body  background="img/bg1.jpg">


<nav class="navbar navbar-default">
<div class="contenedor" class="navbar-header">
<div class="menu-boton" id="menu1"></div>
    <div class="menu-lista" id="menu1">
    <ul>
    <li class="menu-activo"><a href="index">Inicio</a></li>
    <li><a href="directivo.php">Directivos</a></li>
    <li><a href="#">Rutas</a></li>
    <li><a href="#">Llegar al Aeropuerto</a></li>
    <li><a href="#">Aeropuertos</a></li>
    <li><a href="#">Nuestra flota</a></li>
    <li><a href="reserva.php">Haga su Reserva</a></li>
</ul>
</div>

    </div>

        </nav>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.1.min.js"><\/script>')</script>

<script src="js/vendor/bootstrap.js"></script>
<script src="js/main.js"></script>
</body>
</html>