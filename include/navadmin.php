
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Tangerine">
        <link rel="stylesheet" href="css/bootstrap.css">
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    <title>Administrador</title>
</head>
<body  background="img/bg1.jpg">
<!--muen principal
<nav class="menu ">
    <div class="contenedor">
<div class="menu-boton" id="menu1"></div>
    <div class="menu-lista" id="menu1">
    <ul>
    <li class="menu-activo"><a href="direcagregar.php">Agregar Directivo</a></li>
    <li><a href="record.php">Modificar o eliminar Directivo</a></li>

     <li class="menu-activo"><a href="agregaravion.php">Agregar Avion</a></li>
    <li><a href="recordavion.php">Modificar o eliminar Avion</a></li>

      <li class="menu-activo"><a href="agregarrutas.php">Agregar Ruta</a></li>
    <li><a href="recordrutas.php">Modificar o eliminar Ruta</a></li>

       <li class="menu-activo"><a href="agregarhora.php">Agregar Hora limite</a></li>
    <li><a href="recordhora.php">Modificar o eliminar Hora limite</a></li>

         <li class="menu-activo"><a href="agregaraeropuerto.php">Agregar Aeropuerto</a></li>
    <li><a href="recordaeropuerto.php">Modificar o eliminar Aeropuerto</a></li>
</ul>
</div>

    </div>
</nav> 
-->


<!--menu secundario-->
<nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Cambiar Navegacion</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="adminlloyd.php" class="navbar-brand">LAB</a></div>
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav">
                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Directivos<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="direcagregar.php">Agregar</a></li>
                                <li><a href="record.php"> Modificar</a></li>
                                <li><a href="record.php">Eliminar</a></li>
        
                            </ul>
                        </li>


                         <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Avion<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                            <li><a href="agregaravion.php">Agregar</a></li>
                                <li><a href="recordavion.php"> Modificar</a></li>
                                <li><a href="recordavion.php">Eliminar</a></li>
                            </ul>
                        </li>


                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Rutas<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                            <li><a href="agregarrutas.php">Agregar</a></li>
                                <li><a href="recordrutas.php"> Modificar</a></li>
                                <li><a href="recordrutas.php">Eliminar</a></li>
                                
                            </ul>
                        </li>


                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Hora Limite<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                            <li><a href="agregarhora.php">Agregar</a></li>
                                <li><a href="recordhora.php"> Modificar</a></li>
                                <li><a href="recordhora.php">Eliminar</a></li>
                            </ul>
                        </li>


                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Aeropuertos<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                            <li><a href="agregaraeropuerto.php">Agregar</a></li>
                                <li><a href="recordaeropuerto.php"> Modificar</a></li>
                                <li><a href="recordaeropuerto.php">Eliminar</a></li>
                              
                            </ul>
                        </li>


  <li class="menu-activo"><a href="index.php">Home</a></li>





                    </ul>
                </div>

        </nav>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.1.min.js"><\/script>')</script>

<script src="js/vendor/bootstrap.js"></script>
<script src="js/main.js"></script>
</body>
</html>