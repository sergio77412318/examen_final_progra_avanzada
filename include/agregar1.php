<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Agregar</title>
    <link rel="shortcut icon"  href="img/favicon.jpg">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="icon/style.css">
    <link rel="stylesheet" href="css/style.css">
    
</head>
<body>
    
<div class="container-form">
        <div class="header">
            <div class="logo-title">
                <img src="image/logo_magtimus.png" alt="">
                <h2>Agregar</h2>
            </div>
           
        </div>
        
        <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post" class="form">
            <div class="welcome-form"><h1>Bienvenido</h1><h2></h2></div>
            
            <div class="nombre line-input">
                <label class="lnr lnr-envelope"></label>
                <input type="text" placeholder="nombre" name="nombre">
            </div>
            <div class="cargo line-input">
                <label class="lnr lnr-user"></label>
                <input type="text" placeholder="cargo" name="cargo">
            </div>
            <div class="email line-input">
                <label class="lnr lnr-lock"></label>
                <input type="email" placeholder="email" name="email">
            </div>
            
            <?php if(!empty($error)): ?>
            <div class="mensaje">
                <?php echo $error; ?>
            </div>
            <?php endif; ?>
            
            <button type="submit">Agregar<label class="lnr lnr-chevron-right"></label></button>
               
    </form>
    </div>

    
    <script src="js/jquery.js"></script>
    <script src="js/script.js"></script>
</body>
</html>