<!doctype html>
<html lang="en">
  <head>
    
        <link rel="stylesheet" href="css/bootstrap.css">
       
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    <title>Agregar</title>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon"  href="img/favicon.jpg">
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  </head>
  <body class="bg-info">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="adminlloyd.php">Home</a>
  
  <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Directivos<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="direcagregar.php">Agregar</a></li>
                                <li><a href="record.php"> Modificar</a></li>
                                <li><a href="record.php">Eliminar</a></li>
        
                            </ul>
                        </li>

                         <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Avion<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                            <li><a href="agregaravion.php">Agregar</a></li>
                                <li><a href="recordavion.php"> Modificar</a></li>
                                <li><a href="recordavion.php">Eliminar</a></li>
                            </ul>
                        </li>


                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Rutas<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                            <li><a href="agregarrutas.php">Agregar</a></li>
                                <li><a href="recordrutas.php"> Modificar</a></li>
                                <li><a href="recordrutas.php">Eliminar</a></li>
                                
                            </ul>
                        </li>


                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Hora Limite<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                            <li><a href="agregarhora.php">Agregar</a></li>
                                <li><a href="recordhora.php"> Modificar</a></li>
                                <li><a href="recordhora.php">Eliminar</a></li>
                            </ul>
                        </li>


                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Aeropuertos<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                            <li><a href="agregaraeropuerto.php">Agregar</a></li>
                                <li><a href="recordaeropuerto.php"> Modificar</a></li>
                                <li><a href="recordaeropuerto.php">Eliminar</a></li>
                              
                            </ul>
                        </li>

</nav>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.1.min.js"><\/script>')</script>

<script src="js/vendor/bootstrap.js"></script>
<script src="js/main.js"></script>