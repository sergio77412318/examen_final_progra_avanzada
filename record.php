<?php
require 'db.php';
$sql = 'SELECT * FROM directivos';
$statement = $connection->prepare($sql);
$statement->execute();
$directivos = $statement->fetchAll(PDO::FETCH_OBJ);
 ?>
<?php require 'include/navadmin.php'; ?>
<div class="container">
  <div class="card mt-5">
    <div class="card-header">
      <h2 style="color:white" >Listado de Directivos</h2>
    </div>
    <div class="card-body">
      <table class="table table-bordered">
        <tr>
          <th style="color:white">ID</th>
          <th style="color:white">Nombre</th>
          <th style="color:white">Cargo</th>
          <th style="color:white">Email</th>
          <th style="color:white">Action</th>
        </tr>
        <?php foreach($directivos as $person): ?>
          <tr>
            <td style="color:white"><?= $person->id; ?></td>
            <td style="color:white"><?= $person->nombre; ?></td>
            <td style="color:white"><?= $person->cargo; ?></td>
            <td style="color:white"><?= $person->email; ?></td>
            <td>
              <a href="edit.php?id=<?= $person->id ?>" class="btn btn-info">Editar</a>
              <a onclick="return confirm('Seguro que desea eliminar este elemento?')" href="delete.php?id=<?= $person->id ?>" class='btn btn-danger'>Borrar</a>
            </td>
          </tr>
        <?php endforeach; ?>
      </table>
    </div>
  </div>
  <?php require 'include/footer.php'; ?>
</div>









