<?php
require 'db.php';
$sql = 'SELECT * FROM hora';
$statement = $connection->prepare($sql);
$statement->execute();
$hora = $statement->fetchAll(PDO::FETCH_OBJ);
 ?>
<?php require 'include/navadmin.php'; ?>
<div class="container">
  <div class="card mt-5">
    <div class="card-header">
      <h2  style="color:white">Listado de horas limites</h2>
    </div>
    <div class="card-body">
      <table class="table table-bordered">
        <tr>
          <th  style="color:white">ID</th>
          <th  style="color:white">Aeropuertos</th>
          <th  style="color:white">Hora sugerida en Aeropuerto</th>
          <th  style="color:white">Hora limite Check-In(Cierre de vuelo)</th>
          <th  style="color:white">Inicio de Embarque</th>
          <th  style="color:white">Action</th>
        </tr>
        <?php foreach($hora as $person): ?>
          <tr>
            <td  style="color:white"><?= $person->id; ?></td>
            <td  style="color:white"><?= $person->aeropuertos; ?></td>
            <td  style="color:white"><?= $person->sugerida; ?></td>
            <td  style="color:white"><?= $person->limite; ?></td>
            <td  style="color:white"><?= $person->inicio; ?></td>
            <td>
              <a href="edithora.php?id=<?= $person->id ?>" class="btn btn-info">Editar</a>
              <a onclick="return confirm('Seguro que desea eliminar este elemento?')" href="deletehora.php?id=<?= $person->id ?>" class='btn btn-danger'>Borrar</a>
            </td>
          </tr>
        <?php endforeach; ?>
      </table>
    </div>
  </div>
  <?php require 'include/footer.php'; ?>
</div>
