<?php
require 'db.php';
$sql = 'SELECT * FROM avion';
$statement = $connection->prepare($sql);
$statement->execute();
$avion = $statement->fetchAll(PDO::FETCH_OBJ);
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Aviones</title>
    <link rel="shortcut icon"  href="img/favicon.jpg">
    <link rel="stylesheet" href="css/base.css">
    <link rel="stylesheet" href="css/estilos.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
     <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Tangerine">
        <link rel="stylesheet" href="css/bootstrap.css">
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
</head>
<body>
<?php include('include/header.php');?>
<?php include('include/nav.php');?>
  
 
    
  
  <div class="contenedor fondo-blanco relleno-8 borde-gris" style="min-height: 900px">
  
  <div style="background-image: url('img/aw.jpg'); width: 100%; height: 100%; " class="columna columna-m-12 columna-g-12">
  <h1 style="text-align:center">Aviones</h1>
  
  <div class="container">
  <div class="card mt-5">
    <div class="card-header">
      
    </div>
    <div class="card-body">
      <table class="table table-bordered">
        <tr>
  
          <th style="color:negro">Nombre</th>
          <th style="color:negro">Tipo</th>
          <th style="color:negro">Proveedor</th>
          <th style="color:negro">Capacida de pasajeros</th>
       
        </tr>
        <?php foreach($avion as $person): ?>
          <tr>
           
            <td style="color:negro"><?= $person->nombre; ?></td>
            <td style="color:negro"><?= $person->tipo; ?></td>
            <td style="color:negro"><?= $person->proveedor; ?></td>
            <td style="color:negro"><?= $person->pasajeros; ?></td>
            
          </tr>
        <?php endforeach; ?>
      </table>
    </div>
  </div>
 
  
  </div>
  </div>
  
     <?php include('include/footer.php');?>
  
  <script src="js/base.js"></script>
</body>
</html>

