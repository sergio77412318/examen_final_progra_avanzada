<?php
require 'db.php';
$sql = 'SELECT * FROM rutas';
$statement = $connection->prepare($sql);
$statement->execute();
$rutas = $statement->fetchAll(PDO::FETCH_OBJ);
 ?>
<?php require 'include/navadmin.php'; ?>
<div class="container">
  <div class="card mt-5">
    <div class="card-header">
      <h2  style="color:white">Modificar</h2>
    </div>
    <div class="card-body">
      <table class="table table-bordered">
        <tr>
          <th  style="color:white">ID</th>
          <th  style="color:white">Salida del vuelo</th>
          <th  style="color:white">Llegada del vuelo</th>
          <th  style="color:white">Costo del pasaje</th>
          <th  style="color:white">Action</th>
        </tr>
        <?php foreach($rutas as $person): ?>
          <tr>
            <td  style="color:white"><?= $person->id; ?></td>
            <td  style="color:white"><?= $person->de; ?></td>
            <td  style="color:white"><?= $person->a; ?></td>
            <td  style="color:white"><?= $person->costo; ?></td>
            <td>
              <a href="editrutas.php?id=<?= $person->id ?>" class="btn btn-info">Editar</a>
              <a onclick="return confirm('Seguro que desea eliminar este elemento?')" href="deleterutas.php?id=<?= $person->id ?>" class='btn btn-danger'>Borrar</a>
            </td>
          </tr>
        <?php endforeach; ?>
      </table>
    </div>
  </div>
  <?php require 'include/footer.php'; ?>
</div>
