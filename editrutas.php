<?php
require 'db.php';
$id = $_GET['id'];
$sql = 'SELECT * FROM rutas  WHERE id=:id';
$statement = $connection->prepare($sql);
$statement->execute([':id' => $id ]);
$person = $statement->fetch(PDO::FETCH_OBJ);
if (isset ($_POST['de']) && isset($_POST['a'])  && isset($_POST['costo']) ) {
  $de = $_POST['de'];
  $a = $_POST['a'];
  $costo = $_POST['costo'];
  $sql = 'UPDATE rutas SET de=:de,a=:a,costo=:costo WHERE id=:id';
  $statement = $connection->prepare($sql);
  if ($statement->execute([':de' => $de, ':a'=>$a, ':costo' => $costo, ':id' => $id])) {
    $message = 'Modificado Correctamente';
  }



}


 ?>
<?php require 'include/navadmin.php'; ?>
<style>
input:invalid {
  border: 1px solid red;
}

input:valid {
  border: 1px solid green;
}
</style>
<div class="container">
  <div class="card mt-5">
    <div class="card-header">
      <h2  style="color:white">Modificar</h2>
    </div>
    <div class="card-body">
      <?php if(!empty($message)): ?>
        <div class="alert alert-success">
          <?= $message; ?>
        </div>
      <?php endif; ?>
      <form method="post">
        <div class="form-group">
        <label for="de"  style="color:white">Salida del vuelo</label><br>
          <input value="<?= $person->de; ?>" type="text" name="de" id="de" class="col-md-6" pattern="<?php [':de' => $de]?>" required><br>
        </div>
        <div class="form-group">
          <label for="a"  style="color:white">Llegada del vuelo</label><br>
          <input value="<?= $person->a; ?>" type="text" name="a" id="a" class="col-md-6" pattern="<?php [':a' => $a]?>" required><br>
        </div>
        <div class="form-group">
          <label for="costo"  style="color:white">Costo del pasaje en bolivianos</label><br>
          <input value="<?= $person->costo; ?>" type="text" name="costo" id="costo" class="xol-md-6" pattern="<?php [':costo' => $costo]?>" required><br>
        </div>
        <div class="form-group">
          <button type="submit" class="btn  btn-info">Editar</button>
        </div>
      </form>
    </div>
  </div>
  <?php require 'include/footer.php'; ?>
</div>
