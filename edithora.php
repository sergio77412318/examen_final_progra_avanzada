<?php
require 'db.php';
$id = $_GET['id'];
$sql = 'SELECT * FROM hora  WHERE id=:id';
$statement = $connection->prepare($sql);
$statement->execute([':id' => $id ]);
$person = $statement->fetch(PDO::FETCH_OBJ);
if (isset ($_POST['aeropuertos']) && isset($_POST['sugerida'])  && isset($_POST['limite'])  && isset($_POST['inicio']) ) {
  $aeropuertos = $_POST['aeropuertos'];
  $sugerida = $_POST['sugerida'];
  $limite = $_POST['limite'];
  $inicio = $_POST['inicio'];
  $sql = 'UPDATE hora SET aeropuertos=:aeropuertos,sugerida=:sugerida,limite=:limite, inicio=:inicio WHERE id=:id';
  $statement = $connection->prepare($sql);
  if ($statement->execute([':aeropuertos' => $aeropuertos, ':sugerida'=>$sugerida, ':limite' => $limite,':inicio'=>$inicio, ':id' => $id])) {
    $message = 'Modificado Correctamente';
  }



}


 ?>
<?php require 'include/navadmin.php'; ?>
<style>
input:invalid {
  border: 1px solid red;
}

input:valid {
  border: 1px solid green;
}
</style>
<div class="container">
  <div class="card mt-5">
    <div class="card-header">
      <h2  style="color:white">Modificar</h2>
    </div>
    <div class="card-body">
      <?php if(!empty($message)): ?>
        <div class="alert alert-success">
          <?= $message; ?>
        </div>
      <?php endif; ?>
      <form method="post">
        <div class="form-group">
        <label for="aeropuertos"  style="color:white">Aeropuertos</label><br>
          <input value="<?= $person->aeropuertos; ?>" type="text" name="aeropuertos" id="aeropuertos" class="col-md-6" pattern="<?php [':aeropuertos' => $aeropuertos]?>" required><br>
        </div>
        <div class="form-group">
          <label for="sugerida"  style="color:white">Hora sugerida en Aeropuerto</label><br>
          <input value="<?= $person->sugerida; ?>" type="text" name="sugerida" id="sugerida" class="col-md-6" pattern="<?php [':sugerida' => $sugerida]?>" required><br>
        </div>
        <div class="form-group">
          <label for="limite"  style="color:white">Hora limite Check-In(Cierre de vuelo)</label><br>
          <input value="<?= $person->limite; ?>" type="text" name="limite" id="limite" class="col-md-6" pattern="<?php [':limite' => $limite]?>" required><br>
        </div>
        <div class="form-group">
          <label for="inicio"  style="color:white">Inicio de Embarque</label><br>
          <input value="<?= $person->inicio; ?>" type="text" name="inicio" id="inicio" class="col-md-6" pattern="<?php [':inicio' => $inicio]?>" required><br>
        </div>
        <div class="form-group">
          <button type="submit" class="btn  btn-info">Editar</button>
        </div>
      </form>
    </div>
  </div>
  <?php require 'include/footer.php'; ?>
</div>
