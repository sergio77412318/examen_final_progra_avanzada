-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 18-06-2018 a las 04:44:44
-- Versión del servidor: 10.1.30-MariaDB
-- Versión de PHP: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `final`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aeropuerto`
--

CREATE TABLE `aeropuerto` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `ciudad` varchar(100) NOT NULL,
  `ubicacion` varchar(100) NOT NULL,
  `contacto` varchar(100) NOT NULL,
  `temperatura` varchar(100) NOT NULL,
  `urlweb` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `aeropuerto`
--

INSERT INTO `aeropuerto` (`id`, `nombre`, `ciudad`, `ubicacion`, `contacto`, `temperatura`, `urlweb`) VALUES
(3, 'viru viru', 'santa cruz', 'trompillo', '3723876', '23 grados', 'www.lab.bo'),
(4, 'viru viru', 'santa cruz', 'el trompillo', '3723876', '25 grado', 'www.lab.bo'),
(5, 'ja', 'la', '350.i', '7695.0', '79@eh', 'vusty35');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `avion`
--

CREATE TABLE `avion` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `tipo` varchar(100) NOT NULL,
  `proveedor` varchar(100) NOT NULL,
  `pasajeros` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `avion`
--

INSERT INTO `avion` (`id`, `nombre`, `tipo`, `proveedor`, `pasajeros`) VALUES
(4, 'B-787-800', 'comercial', 'Boeing', '300'),
(10, 'razon', 'razon', 'razon', '33333333333333333333333333333333333333300000000000000000000000000000000000'),
(11, 'razon', 'razon', 'razon', 'tulp0[----'),
(12, '111', '111', '111', '111@fff'),
(13, 'carlitos', 'carlitos', 'carlitos', 'carlitos'),
(14, 'carlios', 'carlitos', 'carlitos', 'carlitos'),
(15, 'jojo', '3mil', '400', '1'),
(16, 'jojo', '3mil', '400', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `directivos`
--

CREATE TABLE `directivos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `cargo` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `directivos`
--

INSERT INTO `directivos` (`id`, `nombre`, `cargo`, `email`) VALUES
(17, 'sergio andia fernandez', 'gerente', 'sergioandia11@gmail.com'),
(31, 'serch', 'serchs', 'serch@gmail.com'),
(32, 'henry', 'secretario', 'sece@gmail.com'),
(34, 'kdaskljadsjl', 'dassmndasnm,ds', 'sadasds@gmail.com'),
(35, '35```;;\'\'', 'bih\'==', 'ioh--@fo'),
(36, 'wert.\';23', 'gol13.', 'duj.\'@2469'),
(37, '111', '111', '111@fas'),
(38, 'jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj', 'kkkkkkkkkkkkkkkkkkkkkkkkkkkbjvvj', 'rtyubibobobo@ru'),
(39, 'tfguhij', 'ertyuio', 'da@fsf');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hora`
--

CREATE TABLE `hora` (
  `id` int(11) NOT NULL,
  `aeropuertos` varchar(100) NOT NULL,
  `sugerida` varchar(100) NOT NULL,
  `limite` varchar(100) NOT NULL,
  `inicio` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `hora`
--

INSERT INTO `hora` (`id`, `aeropuertos`, `sugerida`, `limite`, `inicio`) VALUES
(5, 'Cochabamba Jorge Wilstermann International Airport', '60 minutos antes del vuelo', '30 minutos antes del vuelo', '25 minutos antes del vuelo'),
(17, 'el alto', '3 horas antes', '4 horas antes', '20 minutos antes'),
(18, '123', '36gh', '45rh', 'hulk'),
(19, '123', 'wet', '124', 'fyg'),
(20, '33333333333333ihoj', 'ggggggggggghiov', 'gggggggggggggpiv', 'civihn');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `login`
--

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `correo` varchar(123) NOT NULL,
  `usuario` varchar(100) NOT NULL,
  `clave` varchar(200) NOT NULL
) ENGINE=MEMORY DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserva`
--

CREATE TABLE `reserva` (
  `id` int(11) NOT NULL,
  `origen` varchar(100) NOT NULL,
  `destino` varchar(100) NOT NULL,
  `idavuelta` varchar(100) NOT NULL,
  `ida` varchar(100) NOT NULL,
  `salida` date NOT NULL,
  `regreso` date NOT NULL,
  `adultos` varchar(100) NOT NULL,
  `ninos` varchar(100) NOT NULL,
  `bebes` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `reserva`
--

INSERT INTO `reserva` (`id`, `origen`, `destino`, `idavuelta`, `ida`, `salida`, `regreso`, `adultos`, `ninos`, `bebes`) VALUES
(1, 'ORU', 'TDD', 'idavuelta', 'ida', '2018-06-16', '2018-06-16', '1', '6', '3');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rutas`
--

CREATE TABLE `rutas` (
  `id` int(11) NOT NULL,
  `de` varchar(100) NOT NULL,
  `a` varchar(100) NOT NULL,
  `costo` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rutas`
--

INSERT INTO `rutas` (`id`, `de`, `a`, `costo`) VALUES
(4, 'Cochabamba', 'Santa Cruz', 150),
(6, 'beni', 'tarija', 231),
(7, 'bnm', '1234weryp.', 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `aeropuerto`
--
ALTER TABLE `aeropuerto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `avion`
--
ALTER TABLE `avion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `directivos`
--
ALTER TABLE `directivos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hora`
--
ALTER TABLE `hora`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `login`
--
ALTER TABLE `login`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `reserva`
--
ALTER TABLE `reserva`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `rutas`
--
ALTER TABLE `rutas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `aeropuerto`
--
ALTER TABLE `aeropuerto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `avion`
--
ALTER TABLE `avion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `directivos`
--
ALTER TABLE `directivos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT de la tabla `hora`
--
ALTER TABLE `hora`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `reserva`
--
ALTER TABLE `reserva`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `rutas`
--
ALTER TABLE `rutas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
