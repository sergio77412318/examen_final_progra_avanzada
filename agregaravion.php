<?php
require 'db.php';
$message = '';
if (isset ($_POST['nombre']) && isset ($_POST['tipo'])  && isset($_POST['proveedor']) && isset($_POST['pasajeros']) ) {
  $nombre = $_POST['nombre'];
  $tipo = $_POST['tipo'];
  $proveedor=$_POST['proveedor'];
  $pasajeros = $_POST['pasajeros'];
  $sql = 'INSERT INTO avion(nombre, tipo,proveedor, pasajeros) VALUES(:nombre, :tipo,:proveedor, :pasajeros)';
  $statement = $connection->prepare($sql);
  if ($statement->execute([':nombre' => $nombre, ':tipo'=>$tipo, ':proveedor' => $proveedor, ':pasajeros'=>$pasajeros ])) {
    $message = 'Agregado Correctamente';
  }



}


 ?>
<?php require 'include/navadmin.php'; ?>
<style>
input:invalid {
  border: 1px solid red;
}

input:valid {
  border: 1px solid green;
}
</style>
<div class="container">
  <div class="card mt-5">
    <div class="card-header">
      <h2  style="color:white">Agregar</h2>
    </div>
    <div class="card-body">
      <?php if(!empty($message)): ?>
        <div class="alert alert-success">
          <?= $message; ?>
        </div>
      <?php endif; ?>
      <form method="post">
        <div class="form-group">
          <label for="nombre"  style="color:white">Nombre</label><br>
          <input type="text" name="nombre" id="nombre" class="col-md-6" pattern="<?php [':nombre' => $nombre]?>"   required><br>
        </div>
        <div class="form-group">
          <label for="tipo"  style="color:white">Tipo de avion</label><br>
          <input type="text" name="tipo" id="tipo" class="col-md-6" pattern="<?php [':tipo' => $tipo]?>"   required><br>
        </div>
        <div class="form-group">
          <label for="proveedor"  style="color:white">Proveedor</label><br>
          <input type="text" name="proveedor" id="proveedor" class="col-md-6" pattern="<?php [':proveedor' => $proveedor]?>"   required><br>
        </div>
        <div class="form-group">
          <label for="pasajeros"  style="color:white">Capacidad de pasajeros</label><br>
          <input type="text" name="pasajeros" id="pasajeros" class="col-md-6" pattern="<?php [':pasajeros' => $pasajeros]?>"   required><br>
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-info">Agregar</button>
        </div>
      </form>
    </div>
  </div>
  <?php require 'include/footer.php'; ?>
</div>
