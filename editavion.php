<?php
require 'db.php';
$id = $_GET['id'];
$sql = 'SELECT * FROM avion  WHERE id=:id';
$statement = $connection->prepare($sql);
$statement->execute([':id' => $id ]);
$person = $statement->fetch(PDO::FETCH_OBJ);
if (isset ($_POST['nombre']) && isset($_POST['tipo'])  && isset($_POST['proveedor'])  && isset($_POST['pasajeros']) ) {
  $nombre = $_POST['nombre'];
  $tipo = $_POST['tipo'];
  $proveedor = $_POST['proveedor'];
  $pasajeros = $_POST['pasajeros'];
  $sql = 'UPDATE avion SET nombre=:nombre,tipo=:tipo,proveedor=:proveedor, pasajeros=:pasajeros WHERE id=:id';
  $statement = $connection->prepare($sql);
  if ($statement->execute([':nombre' => $nombre, ':tipo'=>$tipo, ':proveedor' => $proveedor,':pasajeros'=>$pasajeros, ':id' => $id])) {
    $message = 'Modificado Correctamente';
  }



}


 ?>
<?php require 'include/navadmin.php'; ?>
<style>
input:invalid {
  border: 1px solid red;
}

input:valid {
  border: 1px solid green;
}
</style>
<div class="container">
  <div class="card mt-5">
    <div class="card-header">
      <h2 style="color:white">Modificar</h2>
    </div>
    <div class="card-body">
      <?php if(!empty($message)): ?>
        <div class="alert alert-success">
          <?= $message; ?>
        </div>
      <?php endif; ?>
      <form method="post">
        <div class="form-group">
        <label for="nombre" style="color:white">Nombre</label><br>
          <input value="<?= $person->nombre; ?>" type="text" name="nombre" id="nombre" class="col-md-6" pattern="<?php [':nombre' => $nombre]?>" required><br>
        </div>
        <div class="form-group">
          <label for="tipo" style="color:white">Tipo de avion</label><br>
          <input value="<?= $person->tipo; ?>" type="text" name="tipo" id="tipo" class="col-md-6" pattern="<?php [':tipo' => $tipo]?>" required><br>
        </div>
        <div class="form-group">
          <label for="proveedor" style="color:white">Proveedor</label><br>
          <input value="<?= $person->proveedor; ?>" type="text" name="proveedor" id="proveedor" class="col-md-6" pattern="<?php [':proveedor' => $proveedor]?>" required><br>
        </div>
        <div class="form-group">
          <label for="pasajeros" style="color:white">Capacidad de pasajeros</label><br>
          <input value="<?= $person->pasajeros; ?>" type="text" name="pasajeros" id="pasajeros" class="col-md-6" pattern="<?php [':pasajeros' => $pasajeros]?>" required><br>
        </div>
        <div class="form-group">
          <button type="submit" class="btn  btn-info">Editar</button>
        </div>
      </form>
    </div>
  </div>
  <?php require 'include/footer.php'; ?>
</div>
