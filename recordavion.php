<?php
require 'db.php';
$sql = 'SELECT * FROM avion';
$statement = $connection->prepare($sql);
$statement->execute();
$avion = $statement->fetchAll(PDO::FETCH_OBJ);
 ?>
<?php require 'include/navadmin.php'; ?>
<div class="container">
  <div class="card mt-5">
    <div class="card-header">
      <h2 style="color:white">Listado de aviones</h2>
    </div>
    <div class="card-body">
      <table class="table table-bordered">
        <tr>
          <th style="color:white">ID</th>
          <th style="color:white">Nombre</th>
          <th style="color:white">Tipo</th>
          <th style="color:white">Proveedor</th>
          <th style="color:white">Capacida de pasajeros</th>
          <th style="color:white">Action</th>
        </tr>
        <?php foreach($avion as $person): ?>
          <tr>
            <td style="color:white"><?= $person->id; ?></td>
            <td style="color:white"><?= $person->nombre; ?></td>
            <td style="color:white"><?= $person->tipo; ?></td>
            <td style="color:white"><?= $person->proveedor; ?></td>
            <td style="color:white"><?= $person->pasajeros; ?></td>
            <td>
              <a href="editavion.php?id=<?= $person->id ?>" class="btn btn-info">Editar</a>
              <a onclick="return confirm('Seguro que desea eliminar este elemento?')" href="deleteavion.php?id=<?= $person->id ?>" class='btn btn-danger'>Borrar</a>
            </td>
          </tr>
        <?php endforeach; ?>
      </table>
    </div>
  </div>
  <?php require 'include/footer.php'; ?>
</div>






