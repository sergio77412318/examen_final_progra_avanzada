<?php
require 'db.php';
$message = '';
if (isset ($_POST['nombre']) && isset ($_POST['cargo'])  && isset($_POST['email']) ) {
  $nombre = $_POST['nombre'];
  $cargo = $_POST['cargo'];
  $email = $_POST['email'];
  $sql = 'INSERT INTO directivos(nombre, cargo, email) VALUES(:nombre, :cargo, :email)';
  $statement = $connection->prepare($sql);
  if ($statement->execute([':nombre' => $nombre, ':cargo'=>$cargo, ':email' => $email])) {
    $message = 'Agregado Correctamente';
  }
}


 ?>
<?php require 'include/navadmin.php'; ?>
<style>
input:invalid {
  border: 1px solid red;
}

input:valid {
  border: 1px solid green;
}
</style>
<div class="container">
  <div class="card mt-5">
    <div class="card-header">
      <h2  style="color:white">Agregar</h2>
    </div>
    <div class="card-body">
      <?php if(!empty($message)): ?>
        <div class="alert alert-success">
          <?= $message; ?>
        </div>
      <?php endif; ?>
      <form method="post">
        <div class="form-group">
          <label for="nombre" style="color:white">Nombre</label><br>
          <input type="text" name="nombre" id="nombre" class="col-md-6" pattern="<?php [':nombre' => $nombre]?>"   required><br>
        </div>
        <div class="form-group">
          <label for="cargo" style="color:white">Cargo</label><br>
          <input type="text" name="cargo" id="cargo" class="col-md-6" pattern="<?php  [':cargo' => $cargo]?>" required><br>
        </div>
        <div class="form-group">
          <label for="email" style="color:white">Email</label><br>
          <input type="email" name="email" id="email" class="col-md-6" pattern="<?php  [':cargo' => $cargo]?>" required><br>
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-info" style="color:white" >Agregar</button>
        </div>
        
      </form>
    </div>
    
  </div>
  <?php require 'include/footer.php'; ?>
</div>

