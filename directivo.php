<?php
require 'db.php';
$sql = 'SELECT * FROM directivos';
$statement = $connection->prepare($sql);
$statement->execute();
$directivos = $statement->fetchAll(PDO::FETCH_OBJ);
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Directivos</title>
    <link rel="shortcut icon"  href="img/favicon.jpg">
    <link rel="stylesheet" href="css/base.css">
    <link rel="stylesheet" href="css/estilos.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
     <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Tangerine">
        <link rel="stylesheet" href="css/bootstrap.css">
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
</head>
<body>
<?php include('include/header.php');?>
<?php include('include/nav.php');?>
  
 
    
  
  <div class="contenedor fondo-blanco relleno-8 borde-gris" style="min-height: 900px">
  
  <div style="background-image: url('img/da.jpg'); width: 100%; height: 100%; " class="columna columna-m-12 columna-g-12">
  <h1 style="text-align:center">Directivos</h1>
  


<div class="container">
  <div class="card mt-5">
    <div class="card-header">
      
    </div>
    <div class="card-body">
      <table class="table table-bordered">
        <tr>
         
          <th style="color:black">Nombre</th>
          <th style="color:black">Cargo</th>
          <th style="color:black">Email</th>
          
        </tr>
        <?php foreach($directivos as $person): ?>
          <tr>
          
            <td style="color:black"><?= $person->nombre; ?></td>
            <td style="color:black"><?= $person->cargo; ?></td>
            <td style="color:black"><?= $person->email; ?></td>
           
          </tr>
        <?php endforeach; ?>
      </table>
    </div>
  </div>
  
</div>






  
  </div>
  </div>
  
     <?php include('include/footer.php');?>
  
  <script src="js/base.js"></script>
</body>
</html>















