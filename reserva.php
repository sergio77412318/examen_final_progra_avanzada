<?php
require 'db.php';
$message = '';
if (isset ($_POST['origen']) && isset ($_POST['destino'])  && isset($_POST['idavuelta'])  && isset($_POST['ida'])  && isset($_POST['salida'])  && isset($_POST['regreso'])  && isset($_POST['adultos'])  && isset($_POST['ninos'])  && isset($_POST['bebes']) ) {
  $origen = $_POST['origen'];
  $destino = $_POST['destino'];
  $idavuelta = $_POST['idavuelta'];
  $ida = $_POST['ida'];
  $salida = $_POST['salida'];
  $regreso = $_POST['regreso'];
  $adultos = $_POST['adultos'];
  $ninos = $_POST['ninos'];
  $bebes = $_POST['bebes'];
  $sql = 'INSERT INTO reserva(origen, destino, idavuelta,ida,salida,regreso,adultos,ninos,bebes) VALUES(:origen, :destino, :idavuelta, :ida, :salida, :regreso, :adultos, :ninos, :bebes)';
  $statement = $connection->prepare($sql);
  if ($statement->execute([':origen' => $origen, ':destino'=>$destino, ':idavuelta' => $idavuelta, ':ida'=>$ida, ':salida'=>$salida, ':regreso'=>$regreso, ':adultos'=>$adultos, ':ninos'=>$ninos, ':bebes'=>$bebes])) {
    $message = 'Agregado Correctamente';
  }
}


 ?>
  <div class="card-body">
      <?php if(!empty($message)): ?>
        <div class="alert alert-success">
          <?= $message; ?>
        </div>
      <?php endif; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reserva de pasajes</title>
    <link rel="shortcut icon"  href="img/favicon.jpg">
    <link rel="stylesheet" href="css/base.css">
    <link rel="stylesheet" href="css/estilos.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
     <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Tangerine">
        <link rel="stylesheet" href="css/bootstrap.css">
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
</head>
<body>
<?php include('include/header.php');?>
<?php include('include/nav.php');?>
  
 
    
  
  <div class="contenedor fondo-blanco relleno-8 borde-gris" style="min-height: 900px">
  
   
  
  

  
  <div style="background-image: url('img/reserva.jpg'); width: 100%; height: 100%; " class="columna columna-m-12 columna-g-12">
  
  
  
  
  <h1 style="text-align:center">Reserva de Pasajes</h1>
  <form method="post">
       <!-- <div class="form-group">
          <label for="nombre" style="color:black">Desde</label><br>
          <input type="text" name="nombre" id="nombre" class="col-md-6"><br>
        </div>
        <div class="form-group">
          <label for="cargo" style="color:negro">A</label><br>
          <input type="text" name="cargo" id="cargo" class="col-md-6"><br>
        </div>


        -->

          <table>
                <tr>
                    <td><label for="origen">Desde:</label></td>
                    <td><label for="destino">A:</label></td>
                </tr>
                <tr>
                    <td class='input-container'>
                        <div class="validable">
                            <select id="origen" name="origen" class="origen-destino">
                                <option value=""  id="origen" name="origen">Origen</option>
                                <option value="CCA"  id="origen" name="origen">CHIMORE</option>
                                <option value="CIJ"  id="origen" name="origen">COBIJA</option>
                                <option value="CBB"  id="origen" name="origen">COCHABAMBA</option>
                                <option value="LPB"  id="origen" name="origen">LA PAZ</option>
                                <option value="ORU"  id="origen" name="origen">ORURO</option>
                                <option value="POI"  id="origen" name="origen">POTOSI</option>
                                <option value="VVI"  id="origen" name="origen">SANTA CRUZ</option>
                                <option value="SRE"  id="origen" name="origen">SUCRE</option>
                                <option value="TJA"  id="origen" name="origen">TARIJA</option>
                                <option value="TDD"  id="origen" name="origen">TRINIDAD</option>
                                <option value="UYU"  id="origen" name="origen">UYUNI</option>
                                <option value="BYC"  id="origen" name="origen">YACUIBA</option>
                                <option value="BCN"  id="origen" name="origen">BARCELONA</option>
                                <option value="EZE"  id="origen" name="origen">BUENOS AIRES</option>
                                <option value="LRM"  id="origen" name="origen">LA ROMANA</option>
                                <option value="MAD"  id="origen" name="origen">MADRID</option>
                                <option value="MIA"  id="origen" name="origen">MIAMI</option>
                                <option value="SLA"  id="origen" name="origen">SALTA</option>
                            </select>
                        </div>
                    </td>
                    <td class='input-container'>
                        <div class="validable">
                            <select id="destino" name="destino" class="origen-destino">
                                <option value="" id="destino" name="destino">Destino</option>
                                <option value="CCA" id="destino" name="destino">CHIMORE</option>
                                <option value="CIJ" id="destino" name="destino">COBIJA</option>
                                <option value="CBB" id="destino" name="destino">COCHABAMBA</option>
                                <option value="LPB" id="destino" name="destino">LA PAZ</option>
                                <option value="ORU" id="destino" name="destino">ORURO</option>
                                <option value="POI" id="destino" name="destino">POTOSI</option>
                                <option value="VVI" id="destino" name="destino">SANTA CRUZ</option>
                                <option value="SRE" id="destino" name="destino">SUCRE</option>
                                <option value="TJA" id="destino" name="destino">TARIJA</option>
                                <option value="TDD" id="destino" name="destino">TRINIDAD</option>
                                <option value="UYU" id="destino" name="destino">UYUNI</option>
                                <option value="BYC" id="destino" name="destino">YACUIBA</option>
                                <option value="BCN" id="destino" name="destino">BARCELONA</option>
                                <option value="EZE" id="destino" name="destino">BUENOS AIRES</option>
                                <option value="LRM" id="destino" name="destino">LA ROMANA</option>
                                <option value="MAD" id="destino" name="destino">MADRID</option>
                                <option value="MIA" id="destino" name="destino">MIAMI</option>
                                <option value="SLA" id="destino" name="destino">SALTA</option>
                            </select>
                        </div>
                    </td>
                </tr>
            </table>

























        <div class="form-group">
        <input type="radio" name="idavuelta" value="idavuelta" id="idavuelta" checked>Solo ida<br>
        </div>
        <div class="form-group">
        <input type="radio" name="ida" value="ida" id="ida">Ida y vuelta<br>   
        </div>
        <div class="form-group">
        <tr>
        <td>
        <label for="salida" style="color:negro">Salida</label>
          <input type="date" name="salida" id="salida">
</td>
<td>
 <label for="regreso" style="color:negro">Regreso</label>         
          <input type="date" name="regreso" id="regreso">
    </td>
        </tr>
        </div>



        
        <div class="form-group">
      <tr>
        <td class="input-container">
        <label for="adultos" style="color:black">Adultos</label>
                        <select id="adultos" name="adultos" class="number">
                            <option value="0" selected  id="adultos" name="adultos">0</option>
                            <option value="1"  id="adultos" name="adultos">1</option>
                            <option value="2"  id="adultos" name="adultos">2</option>
                            <option value="3"  id="adultos" name="adultos">3</option>
                            <option value="4"  id="adultos" name="adultos">4</option>
                            <option value="5"  id="adultos" name="adultos">5</option>
                            <option value="6"  id="adultos" name="adultos">6</option>
                            <option value="7"  id="adultos" name="adultos">7</option>
                            <option value="8"  id="adultos" name="adultos">8</option>
                            <option value="9"  id="adultos" name="adultos">9</option>
                        </select>
                    </td>    
       
                    
                    <td class="input-container">
                    <label for="ninos" style="color:black">Niños(2-11)años</label>
                        <select id="ninos" name="ninos" class="number"><br>
                            <option value="0" selected id="ninos" name="ninos">0</option><br>
                            <option value="1" id="ninos" name="ninos">1</option>
                            <option value="2" id="ninos" name="ninos">2</option>
                            <option value="3" id="ninos" name="ninos">3</option>
                            <option value="4" id="ninos" name="ninos">4</option>
                            <option value="5" id="ninos" name="ninos">5</option>
                            <option value="6" id="ninos" name="ninos">6</option>
                            <option value="7" id="ninos" name="ninos">7</option>
                            <option value="8" id="ninos" name="ninos">8</option>
                            <option value="9" id="ninos" name="ninos">9</option>
                        </select>
                    </td>    
       
       
          
                    <td class="input-container">
                    <label for="bebes" style="color:black">Bebes(0-1)años</label>
                        <select id="bebes" name="bebes" class="number"><br>
                            <option value="0" selected id="bebes" name="bebes">0</option><br>
                            <option value="1" id="bebes" name="bebes">1</option>
                            <option value="2" id="bebes" name="bebes">2</option>
                            <option value="3" id="bebes" name="bebes">3</option>
                            <option value="4" id="bebes" name="bebes">4</option>
                            <option value="5" id="bebes" name="bebes">5</option>
                            <option value="6" id="bebes" name="bebes">6</option>
                            <option value="7" id="bebes" name="bebes">7</option>
                            <option value="8" id="bebes" name="bebes">8</option>
                            <option value="9" id="bebes" name="bebes">9</option>
                        </select>
                    </td>    
       
       </tr>
       
        </div>



        







        
        <div class="form-group">
          <button type="submit" class="btn btn-info" >Agregar</button>
        </div>
      </form>
      
  </div>
  

  </div>
  </div>
     <?php include('include/footer.php');?>
  
  <script src="js/base.js"></script>
</body>
</html>
























