<?php
require 'db.php';
$sql = 'SELECT * FROM aeropuerto';
$statement = $connection->prepare($sql);
$statement->execute();
$aeropuerto = $statement->fetchAll(PDO::FETCH_OBJ);
 ?>
<?php require 'include/navadmin.php'; ?>
<div class="container">
  <div class="card mt-5">
    <div class="card-header">
      <h2  style="color:white">Listado de aeropuertos</h2>
    </div>
    <div class="card-body">
      <table class="table table-bordered">
        <tr>
          <th  style="color:white">ID</th>
          <th  style="color:white">Nombre</th>
          <th  style="color:white">Ciudad</th>
          <th  style="color:white">Ubicacion</th>
          <th  style="color:white">Contacto</th>
          <th  style="color:white">Temperatura Promedio</th>
          <th  style="color:white">Urlweb</th>
          <th  style="color:white">Action</th>
        </tr>
        <?php foreach($aeropuerto as $person): ?>
          <tr>
            <td  style="color:white"><?= $person->id; ?></td>
            <td  style="color:white"><?= $person->nombre; ?></td>
            <td  style="color:white"><?= $person->ciudad; ?></td>
            <td  style="color:white"><?= $person->ubicacion; ?></td>
            <td  style="color:white"><?= $person->contacto; ?></td>
            <td  style="color:white"><?= $person->temperatura; ?></td>
            <td  style="color:white"><?= $person->urlweb; ?></td>
            <td>
              <a href="editaeropuerto.php?id=<?= $person->id ?>" class="btn btn-info">Editar</a>
              <a onclick="return confirm('Seguro que desea eliminar este elemento?')" href="deleteaeropuerto.php?id=<?= $person->id ?>" class='btn btn-danger'>Borrar</a>
            </td>
          </tr>
        <?php endforeach; ?>
      </table>
    </div>
  </div>
  <?php require 'include/footer.php'; ?>
</div>
